//
//  ViewController.swift
//  Timer
//
//  Created by バスケス アンジェルメル on 2021/09/10.
//
//こんばんわ,おはよう
import UIKit //ライブラリー　Classの束

//クラスの継承 ViewController(継承するクラスを定義): UIViewController(画面を構成するものを定義したクラス)
class ViewController: UIViewController {

    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var timeBtm: UIButton!
    @IBOutlet weak var pauseBtm: UIButton!
    @IBOutlet weak var resetBtm: UIButton!
    
    var timer : Timer!
    let oneSecond : TimeInterval = 1
    //分
    var minute = 0
    //秒
    var second = 0
    
    //overrideとは上書き　UIViewControllerクラスのviewDidLoadを呼び出し上書きする
    override func viewDidLoad() {
        super.viewDidLoad()//真っ白な画面を作る
        // Do any additional setup after loading the view.
    }
    
    //スタートボタンを押した時
    @IBAction func onClicktimeBtm(_ sender: UIButton) {
        timer = Timer.scheduledTimer(timeInterval: oneSecond, target: self, selector: #selector(runTime), userInfo: nil, repeats: true)
        timeBtm.isEnabled = false
        pauseBtm.isEnabled = true
        resetBtm.isEnabled = true
    }
    
    //一時停止を押した時
    @IBAction func onClickpauseBtm(_ sender: UIButton) {
        timer.invalidate()
        pauseBtm.isEnabled = false
        timeBtm.isEnabled = true
    }
    
    //リセットを押した時
    @IBAction func onClickresetBtm(_ sender: UIButton) {
        timer.invalidate()
        timeBtm.isEnabled = true
        pauseBtm.isEnabled = false
        resetBtm.isEnabled = false
        timeLabel.text = "00:00"
        second = 0
        minute = 0
    }
    
    @objc func runTime() {
        //１秒加算
        second += 1
        //60秒経ったら１分加算して秒を０に戻す
        if second == 60 {
            minute += 1
            second = 0
        }
        var tmpM = ""
        var tmpS = ""
        
        //秒のテキストを制御
        if second < 10 {
            tmpS = "0\(second)"
        } else if second >= 10{
            tmpS = "\(second)"
        }
        
        //分のテキストを制御
        if minute < 10 {
            tmpM = "0\(minute)"
        } else if minute >= 10{
            tmpM = "\(minute)"
        }
        timeLabel.text = "\(tmpM):\(tmpS)"
    }
}

